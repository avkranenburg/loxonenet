﻿namespace Loxone.Net.Data.Api;

public class TokenData
{
    [JsonPropertyName("token")]
    public string Token { get; set; }

    [JsonPropertyName("key")]
    public string Key { get; set; }

    [JsonPropertyName("validUntil")]
    public int ValidUntil { get; set; }

    [JsonPropertyName("tokenRights")]
    public int TokenRights { get; set; }

    [JsonPropertyName("unsecurePass")]
    public bool UnsecurePass { get; set; }
}