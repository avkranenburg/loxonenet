﻿namespace Loxone.Net.Data.Api;

public class ApiResponse <TValue>
{
    [JsonPropertyName("LL")]
    public ApiResponseData<TValue> Data { get; set; }
}

public class ApiResponse
{
    [JsonPropertyName("LL")]
    public ApiResponseData Data { get; set; }
}