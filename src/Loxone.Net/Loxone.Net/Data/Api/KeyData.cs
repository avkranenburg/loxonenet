﻿namespace Loxone.Net.Data.Api;

public class KeyData
{
    [JsonPropertyName("key")]
    public string Key { get; set; }

    [JsonPropertyName("salt")]
    public string Salt { get; set; }

    [JsonPropertyName("hashAlg")]
    public string HashAlgorithm { get; set; }
}