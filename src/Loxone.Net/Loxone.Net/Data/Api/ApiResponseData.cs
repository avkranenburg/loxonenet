﻿namespace Loxone.Net.Data.Api;

public class ApiResponseData <TValue>
{
    [JsonPropertyName("control")]
    public string Control { get; set; }

    [JsonPropertyName("code")]
    public int Code { get; set; }

    [JsonPropertyName("value")]
    public TValue Value { get; set; }
}

public class ApiResponseData
{
    [JsonPropertyName("control")]
    public string Control { get; set; }

    [JsonPropertyName("code")]
    public int Code { get; set; }
}