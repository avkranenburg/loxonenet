﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneDataPoint
{
	[JsonPropertyName("title")]
	public string Title { get; set; }

	[JsonPropertyName("format")]
	public string Format { get; set; }

	[JsonPropertyName("output")]
	public string Output { get; set; }
}