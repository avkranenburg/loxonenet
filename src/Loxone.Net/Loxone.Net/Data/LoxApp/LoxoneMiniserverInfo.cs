﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneMiniserverInfo
{
	[JsonPropertyName("serialNr")]
	public string SerialNumber { get; set; }

	[JsonPropertyName("msName")]
	public string Name { get; set; }

	[JsonPropertyName("projectName")]
	public string ProjectName { get; set; }

	[JsonPropertyName("localUrl")]
	public string LocalUrl { get; set; }

	[JsonPropertyName("remoteUrl")]
	public string RemoteUrl { get; set; }

	[JsonPropertyName("hostname")]
	public string Hostname { get; set; }

	[JsonPropertyName("tempUnit")]
	public int TemperatureUnit { get; set; }

	[JsonPropertyName("currency")]
	public string Currency { get; set; }

	[JsonPropertyName("squareMeasure")]
	public string SquareMeasure { get; set; }

	[JsonPropertyName("location")]
	public string Location { get; set; }

	[JsonPropertyName("latitude")]
	public float Latitude { get; set; }

	[JsonPropertyName("longitude")]
	public float Longitude { get; set; }

	[JsonPropertyName("altitude")]
	public int Altitude { get; set; }

	[JsonPropertyName("languageCode")]
	public string LanguageCode { get; set; }

	[JsonPropertyName("heatPeriodStart")]
	public string HeatPeriodStart { get; set; }

	[JsonPropertyName("heatPeriodEnd")]
	public string HeatPeriodEnd { get; set; }

	[JsonPropertyName("coolPeriodStart")]
	public string CoolPeriodStart { get; set; }

	[JsonPropertyName("coolPeriodEnd")]
	public string CoolPeriodEnd { get; set; }

	[JsonPropertyName("catTitle")]
	public string CategoryTitle { get; set; }

	[JsonPropertyName("roomTitle")]
	public string RoomTitle { get; set; }

	[JsonPropertyName("miniserverType")]
	public int MiniserverType { get; set; }

	[JsonPropertyName("deviceMonitor")]
	public string DeviceMonitor { get; set; }

	[JsonPropertyName("currentUser")]
	public LoxoneUser CurrentUser { get; set; }
}