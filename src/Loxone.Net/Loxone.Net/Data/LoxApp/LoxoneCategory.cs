﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneCategory
{
	[JsonPropertyName("uuid")]
	public string Uuid { get; set; }

	[JsonPropertyName("name")]
	public string Name { get; set; }

	[JsonPropertyName("image")]
	public string Image { get; set; }

	[JsonPropertyName("defaultRating")]
	public int DefaultRating { get; set; }

	[JsonPropertyName("isFavorite")]
	public bool IsFavorite { get; set; }

	[JsonPropertyName("type")]
	public string Type { get; set; }

	[JsonPropertyName("color")]
	public string Color { get; set; }
}