﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneStatisticV2
{
	[JsonPropertyName("groups")]
	public List<LoxoneStatisticV2Group> Groups { get; set; }
}