﻿namespace Loxone.Net.Data.LoxApp;

public class LoxApp3
{
	[JsonPropertyName("lastModified")]
	public DateTime LastModified { get; set; }

	[JsonPropertyName("msInfo")]
	public LoxoneMiniserverInfo MiniserverInfo { get; set; }

	[JsonPropertyName("globalStates")]
	public Dictionary<string, string> GlobalStates { get; set; }

	[JsonPropertyName("operatingModes")]
	public Dictionary<string, string> OperatingModes { get; set; }

	[JsonPropertyName("rooms")]
	public Dictionary<string, LoxoneRoom> Rooms { get; set; }

	[JsonPropertyName("cats")]
	public Dictionary<string, LoxoneCategory> Categories { get; set; }

	[JsonPropertyName("controls")]
	public Dictionary<string, LoxoneControl> Controls { get; set; }

	public bool IsValid => LastModified != DateTime.UnixEpoch;
}