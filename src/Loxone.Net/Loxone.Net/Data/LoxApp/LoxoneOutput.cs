﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneOutput
{
	[JsonPropertyName("id")]
	public int Id { get; set; }

	[JsonPropertyName("name")]
	public string Name { get; set; }

	[JsonPropertyName("format")]
	public string Format { get; set; }

	[JsonPropertyName("uuid")]
	public string Uuid { get; set; }

	[JsonPropertyName("visuType")]
	public int VisuType { get; set; }
}