﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneStatisticV2Group
{
	[JsonPropertyName("id")]
	public string Id { get; set; }

	[JsonPropertyName("mode")]
	public int Mode { get; set; }

	[JsonPropertyName("dataPoints")]
	public List<LoxoneDataPoint> DataPoints { get; set; }
}