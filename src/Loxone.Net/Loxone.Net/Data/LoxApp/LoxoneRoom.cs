﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneRoom
{
	[JsonPropertyName("uuid")]
	public string Uuid { get; set; }

	[JsonPropertyName("name")]
	public string Name { get; set; }

	[JsonPropertyName("image")]
	public string Image { get; set; }

	[JsonPropertyName("defaultRating")]
	public int DefaultRating { get; set; }

	[JsonPropertyName("isFavorite")]
	public bool IsFavorite { get; set; }

	[JsonPropertyName("type")]
	public int Type { get; set; }
}