﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneStatistic
{
	[JsonPropertyName("frequency")]
	public int Frequency { get; set; }

	[JsonPropertyName("outputs")]
	public List<LoxoneOutput> Outputs { get; set; }
}