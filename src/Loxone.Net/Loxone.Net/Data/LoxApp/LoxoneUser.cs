﻿namespace Loxone.Net.Data.LoxApp;

public class LoxoneUser
{
	[JsonPropertyName("name")]
	public string Name { get; set; }

	[JsonPropertyName("uuid")]
	public string Uuid { get; set; }

	[JsonPropertyName("isAdmin")]
	public bool IsAdmin { get; set; }

	[JsonPropertyName("changePassword")]
	public bool ChangePassword { get; set; }

	[JsonPropertyName("userRights")]
	public long UserRights { get; set; }
}