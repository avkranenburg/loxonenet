﻿using System.Text.Json;

namespace Loxone.Net.Data.LoxApp;

public class LoxoneControl
{
	[JsonPropertyName("name")]
	public string Name { get; set; }

	[JsonPropertyName("type")]
	public string Type { get; set; }

	[JsonPropertyName("uuidAction")]
	public string UuidAction { get; set; }

	[JsonPropertyName("room")]
	public string RoomUuid { get; set; }

	[JsonPropertyName("cat")]
	public string CategoryUuid { get; set; }

	[JsonPropertyName("defaultRating")]
	public int DefaultRating { get; set; }

	[JsonPropertyName("isFavorite")]
	public bool IsFavorite { get; set; }

	[JsonPropertyName("isSecured")]
	public bool IsSecured { get; set; }

	[JsonPropertyName("links")]
	public List<string> Links { get; set; } = new();

	[JsonPropertyName("states")]
	public Dictionary<string, string> States { get; set; }

	[JsonPropertyName("details")]
	public Dictionary<string, JsonElement>? Details { get; set; }

	[JsonPropertyName("subControls")]
	public Dictionary<string, LoxoneControl>? SubControls { get; set; }

	[JsonPropertyName("statistic")]
	public LoxoneStatistic? Statistics { get; set; }

	[JsonPropertyName("statisticV2")]
	public LoxoneStatisticV2? StatisticsV2 { get; set; }

	public bool UseStatisticV2 => StatisticsV2 != null;
}