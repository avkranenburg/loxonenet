namespace Loxone.Net.Data.Messages;

public class OnMessageEventArgs(BinaryMessage message) : EventArgs
{
	public BinaryMessage Message { get; } = message;
}