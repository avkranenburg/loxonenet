namespace Loxone.Net.Data.Messages;

public enum HeaderIdentifier
{
	TextMessage = 0,
	BinaryFile = 1,
	EventTableOfValueStates = 2,
	EventTableOfTextStates = 3,
	EventTableOfDaytimerStates = 4,
	OutOfService = 5,
	KeepAlive = 6,
	EventTableOfWeatherStates = 7
}