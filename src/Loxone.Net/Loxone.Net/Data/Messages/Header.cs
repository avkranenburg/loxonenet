namespace Loxone.Net.Data.Messages;

public class Header
{
	public HeaderIdentifier Identifier { get; set; }
	public byte Info { get; set; }
	public uint DataLength { get; set; }
	public bool IsEstimate { get; set; }
	public int TotalSize => 8 + (int)DataLength;

	public Header (Span<byte> data)
	{
		if (data == null) throw new ArgumentException("Header must be set");
		if (data.Length != 8) throw new ArgumentException("Header must have size of 8");

		Identifier = (HeaderIdentifier)data[1];
		Info = data[2];
		IsEstimate = ((data[3] & 0x80) == 0x80);

		var len = data.Slice(4, 4);
		DataLength = BitConverter.ToUInt32(len.ToArray(), 0);
	}
}