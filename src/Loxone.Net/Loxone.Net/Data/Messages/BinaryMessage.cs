﻿namespace Loxone.Net.Data.Messages;

public abstract class BinaryMessage (Header header)
{
	public Header Header { get; } = header;
}