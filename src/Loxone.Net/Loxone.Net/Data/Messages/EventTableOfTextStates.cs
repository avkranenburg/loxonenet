﻿namespace Loxone.Net.Data.Messages;

public class TextEventData
{
	public TextEventData (string iconId, string text)
	{
		IconId = iconId;
		Text = text;
	}

	public string IconId { get; }
	public string Text { get; }
}

public class EventTableOfTextStates : EventTable<TextEventData>
{
	public EventTableOfTextStates (Header header) : base(header)
	{
	}
}