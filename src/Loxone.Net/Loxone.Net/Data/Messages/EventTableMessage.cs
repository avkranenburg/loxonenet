namespace Loxone.Net.Data.Messages;

public class EventTable<T> (Header header) : BinaryMessage(header)
{
	public Dictionary<string, T> Values { get; } = new();
}