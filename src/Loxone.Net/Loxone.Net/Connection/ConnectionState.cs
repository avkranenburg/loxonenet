﻿namespace Loxone.Net.Connection;

public enum ConnectionState
{
    Connecting,
    AcquiringToken,
    SaveToken,
    SendEnableUpdates,
    RefreshingToken,
    Connected,
    Disconnected
}