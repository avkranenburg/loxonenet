﻿using System.Diagnostics;
using System.Net.WebSockets;
using System.Security.Cryptography;
using System.Text;
using System.Timers;
using FluentResults;
using Loxone.Net.Data.Api;
using Loxone.Net.Data.Messages;
using Loxone.Net.Extensions;
using PureWebSockets;
using Timer = System.Timers.Timer;

namespace Loxone.Net.Connection;

public class LoxoneWebsocket
{
	private const string LoxoneUuid = "839aa1bf-1f5d-45dc-becc-c653939bff1e";

	private readonly PureWebSocket _webSocket;
	private readonly Timer _keepAliveTimer = new(TimeSpan.FromMinutes(4).TotalMilliseconds);
	private ConnectionState _connectionState = ConnectionState.Disconnected;
	private readonly string _username;
	private readonly string _password;
	private TokenData? _currentToken;
	private HashAlgorithm? _hashAlgorithm;
	private readonly List<byte> _buffer = new();
	private readonly object _lock = new();

	public event EventHandler<OnMessageEventArgs>? OnMessageReceived;

	public bool IsConnected => (_connectionState == ConnectionState.Connected);

	public LoxoneWebsocket (string hostname, int port, string username, string password)
	{
		_username = username;
		_password = password;

		var socketOptions = new PureWebSocketOptions
		{
			MyReconnectStrategy = new ReconnectStrategy(5, 10, 5),
		};

		_webSocket = new PureWebSocket($"wss://{hostname}:{port}/ws/rfc6455", socketOptions);
		_keepAliveTimer.Elapsed += _keepAliveTimer_Elapsed;
	}

	public async Task<Result> ConnectAsync ()
	{
		_webSocket.OnMessage += _webSocket_OnMessage;
		_webSocket.OnData += _webSocket_OnData;

		_connectionState = ConnectionState.Connecting;
		var connected = await _webSocket.ConnectAsync();

		if (connected) GetUserKey();
		if (connected) _keepAliveTimer.Start();
		return Result.OkIf(connected, "Could not establish connection");
	}

	public void Close ()
	{
		_webSocket.OnMessage -= _webSocket_OnMessage;
		_webSocket.OnData -= _webSocket_OnData;

		_connectionState = ConnectionState.Disconnected;
		_webSocket.Disconnect();
	}

	public bool EnableStatusUpdates ()
	{
		return _webSocket.Send("jdev/sps/enablebinstatusupdate");
	}

	private void _webSocket_OnData (object _, byte[] data)
	{
		lock (_lock)
		{
			_buffer.AddRange(data);
			TryReadData();
		}
	}

	private void TryReadData ()
	{
		const int headerSize = 8;

        // check if buffer contains anything
		if (_buffer.Count <= 0) return;
        // check for header marker
		if (_buffer[0] != 0x03) return;
        // check if buffer is large enough for the header
		if (_buffer.Count < headerSize) return;

		var dataBuf = _buffer.ToArray().AsSpan();
		var header = new Header(dataBuf[0..headerSize]);

		if (header.IsEstimate)
		{
			// ignore estimates
			SkipMessageBytes(dataBuf, headerSize);
		}
		else if (header.Identifier == HeaderIdentifier.TextMessage)
		{
			// clear current message out of the buffer
			SkipMessageBytes(dataBuf, headerSize);
		}
		else
		{
			// ensure that the buffer is completely full
			if (_buffer.Count < header.DataLength + headerSize) return;

			// get relevant data array out of the buffer
			var dataBuffer = dataBuf.Slice(headerSize, (int)header.DataLength);
			var parseMessage = header.ParseMessage(dataBuffer);

			if (parseMessage.IsSuccess)
				OnMessageReceived?.Invoke(this, new OnMessageEventArgs(parseMessage.Value));

			// clear current message out of the buffer
			SkipMessageBytes(dataBuf, header.TotalSize);
		}
	}

	private void SkipMessageBytes(Span<byte> dataBuf, int bytes)
	{
		var nextData = dataBuf.SkipBytes(bytes);
		_buffer.Clear();
		_buffer.AddRange(nextData.ToArray());
	}

	private void _webSocket_OnMessage (object _, string message)
	{
		switch (_connectionState)
		{
			case ConnectionState.AcquiringToken:
				GetToken(message);
				return;
			case ConnectionState.SaveToken:
			case ConnectionState.RefreshingToken:
				SaveToken(message);
				return;
			case ConnectionState.Connected:
			case ConnectionState.SendEnableUpdates:
			case ConnectionState.Disconnected:
			case ConnectionState.Connecting:
			default:
				return;
		}
	}

	private void GetToken (string message)
	{
		var data = LoxoneJson.Deserialize<ApiResponse<KeyData>>(message);
		if (data == null)
		{
			// todo: handle retry
			Close();
			return;
		}

		_hashAlgorithm = data.Data.Value.HashAlgorithm switch
		{
			"SHA1" => SHA1.Create(),
			"SHA256" => SHA256.Create(),
			_ => SHA256.Create()
		};

		var pwHash = _hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes($"{_password}:{data.Data.Value.Salt}")).ToHex(true);
		var hash = $"{_username}:{pwHash}".CalculateHmac(_hashAlgorithm, data.Data.Value.Key.ToByteArray());

		_webSocket.Send($"jdev/sys/getjwt/{hash}/{_username}/4/{LoxoneUuid}/LoxoneNet");
		_connectionState = ConnectionState.SaveToken;
	}

	private void SaveToken(string message)
	{
		var result = LoxoneJson.Deserialize<ApiResponse<TokenData>>(message);
		if (result == null)
		{
			// todo: handle retry
			Close();
			return;
		}

		_connectionState = result.Data.Code == 200 ? ConnectionState.Connected : ConnectionState.Disconnected;
		_currentToken = result.Data.Value;
	}

	private void GetUserKey ()
	{
		_webSocket.SendAsync($"jdev/sys/getkey2/{_username}");
		_connectionState = ConnectionState.AcquiringToken;
	}

	private void RefreshToken ()
	{
		// todo: check token hashing and request refresh
	}

	private void _keepAliveTimer_Elapsed (object? _, ElapsedEventArgs e)
	{
		if (_webSocket.State != WebSocketState.Open) return;
		_webSocket.Send("keepalive");

		if (_currentToken?.ValidUntil.ToDateTime() < DateTime.Now + TimeSpan.FromMinutes(5))
			GetUserKey();
	}
}