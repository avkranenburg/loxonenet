﻿using FluentResults;
using Loxone.Net.Data.Api;
using RestSharp;
using RestSharp.Authenticators;

namespace Loxone.Net.Connection;

public class LoxoneWebApi
{
    private RestClient _client;

    public LoxoneWebApi (string host, int port, string username, string password)
    {
        var options = new RestClientOptions($"https://{host}:{port}")
        {
            Authenticator = new HttpBasicAuthenticator(username, password)
        };

        _client = new RestClient(options);
    }

    public async Task<Result<T>> GetAsync<T> (string url)
    {
        var request = new RestRequest(url);
        var response = await _client.ExecuteAsync(request);
        if (response.IsSuccessful)
            return Result.Ok<T>(LoxoneJson.Deserialize<T>(response.Content));
        return Result.Fail($"Server responded with status code: {response.StatusCode}");
    }

    public async Task<Result<string>> WriteValue (string uuid, object value)
    {
        string param;

        if (value is bool bValue)
            param = bValue ? "on" : "off";
        else
            param = value.ToString();

        var request = new RestRequest($"/jdev/sps/io/{uuid}/{param}");
        var response = await _client.ExecuteAsync(request);
        if (response.IsSuccessful)
            return Result.Ok(LoxoneJson.Deserialize<ApiResponse<string>>(response.Content).Data.Value);
        return Result.Fail($"Server responded with status code: {response.StatusCode}");
    }

    public async Task<Result<string>> ReadValue (string uuid)
    {
        var request = new RestRequest($"/jdev/sps/io/{uuid}");
        var response = await _client.ExecuteAsync(request);
        if (response.IsSuccessful)
            return Result.Ok(LoxoneJson.Deserialize<ApiResponse<string>>(response.Content).Data.Value);
        return Result.Fail($"Server responded with status code: {response.StatusCode}");
    }
}