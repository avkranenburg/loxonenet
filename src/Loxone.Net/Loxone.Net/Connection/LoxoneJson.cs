﻿using System.Text.Json;

namespace Loxone.Net.Connection;

public class LoxoneJson
{
    private static readonly JsonSerializerOptions Options = new JsonSerializerOptions()
    {
        PropertyNameCaseInsensitive = true,
        NumberHandling = JsonNumberHandling.AllowReadingFromString,
        AllowTrailingCommas = true,
        Converters = { new LoxoneDateParser() }
    };

    public static T? Deserialize<T> (string json)
    {
        return JsonSerializer.Deserialize<T>(json, Options);
    }
}

internal class LoxoneDateParser : JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var input = reader.GetString();
        if (input == null) return default;
        return DateTime.TryParse(input, out var result) ? result : default;
    }

    public override void Write (Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString("O"));
    }
}