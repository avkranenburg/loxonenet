using System.Security.Cryptography;
using System.Text;

namespace Loxone.Net.Extensions;

public static class HashExtensions
{
	public static string ToHex (this byte[] bytes, bool upperCase)
	{
		var result = new StringBuilder(bytes.Length * 2);

		for (var i = 0; i < bytes.Length; i++)
			result.Append(bytes[i].ToString(upperCase ? "X2" : "x2"));

		return result.ToString();
	}
	
	public static string CalculateHmac (this string text, HashAlgorithm hasher, byte[] key)
	{
		HMAC hmac = hasher switch
		{
			SHA1 => new HMACSHA1(key),
			SHA256 => new HMACSHA256(key),
			_ => new HMACSHA256(key),
		};
		hmac.Initialize();
		
		var byteArray = Encoding.UTF8.GetBytes(text);
		var hashArray = hmac.ComputeHash(byteArray);
		return hashArray.ToHex(false);
	}
}