namespace Loxone.Net.Extensions;

public static class ByteArrayExtensions
{
	public static string ByteArrayToString (this byte[] data)
	{
		return data == null ? string.Empty : BitConverter.ToString(data).ToLower().Replace("-", "");
	}
	
	public static string ByteArrayToString (this ReadOnlySpan<byte> data)
	{
		var arr = data.ToArray();
		return arr.ByteArrayToString();
	}
	
	public static string ByteArrayToString (this Span<byte> data)
	{
		var arr = data.ToArray();
		return arr.ByteArrayToString();
	}
	
	public static byte[] ToByteArray (this string hex)
	{
		return Enumerable.Range(0, hex.Length)
			.Where(x => x % 2 == 0)
			.Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
			.ToArray();
	}

	public static Span<byte> SkipBytes (this Span<byte> data, int bytes) 
	{
		return data.Slice(bytes, data.Length - bytes);
	}
}