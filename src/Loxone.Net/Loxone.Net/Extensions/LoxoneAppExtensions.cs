﻿using System.Drawing;
using Loxone.Net.Application;
using Loxone.Net.Application.Controls;
using Loxone.Net.Application.States;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;

namespace Loxone.Net.Extensions;

public static class LoxoneAppExtensions
{
    public static LoxoneApp CreateApp (this LoxApp3 data, LoxoneWebApi webApi)
    {
        var result = new LoxoneApp(webApi, data.LastModified);

        // add categories
        result.Categories.AddRange(data.Categories.Select(x => x.Value.Convert(webApi)));

        // add rooms
        result.Rooms.AddRange(data.Rooms.Select(x => x.Value.Convert(webApi)));

        // add controls
        result.Controls.AddRange(data.Controls.Select(x => x.Value.Convert(result, webApi)));

        // add operating modes
        foreach (var operatingMode in data.OperatingModes)
        {

        }

        // link controls
        data.Controls
            .Where(x => x.Value.Links.Count != 0)
            .ToList()
            .ForEach(x =>
            {
                var control = result.GetControl(x.Key);
                if (control == null) return;
                x.Value.Links.ForEach(l =>
                {
                    var linked = result.GetControl(l);
                    if (linked != null)
                        control.LinkedControls.Add(linked);
                });
            });

        // fill room/category control lists
        result.Controls.ForEach(x =>
        {
            x.Category?.Controls.Add(x);
            x.Room?.Controls.Add(x);
        });

        return result;
    }

    private static Category Convert (this LoxoneCategory category, LoxoneWebApi webApi)
    {
        return new Category(category.Uuid, category.Name, webApi)
        {
            Color = Color.Red
        };
    }

    private static Room Convert (this LoxoneRoom category, LoxoneWebApi webApi)
    {
        return new Room(category.Uuid, category.Name, webApi)
        {

        };
    }

    internal static Control Convert (this LoxoneControl control, LoxoneApp app, LoxoneWebApi webApi) => (control.Type) switch
    {
        "Jalousie" => new Jalousie(control, webApi, app),
        "Switch" => new Switch(control, webApi, app),
        "PresenceDetector" => new PresenceDetector(control, webApi, app),
        _ => new Control(control, webApi, app)
    };

    public static Control? GetControl(this LoxoneApp app, string Uuid) => app.Controls.FirstOrDefault(x => x.Uuid == Uuid);
}