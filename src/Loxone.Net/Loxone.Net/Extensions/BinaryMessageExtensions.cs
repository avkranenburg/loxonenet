﻿using System.Text;
using FluentResults;
using Loxone.Net.Data.Messages;

namespace Loxone.Net.Extensions;

public static class BinaryMessageExtensions
{
	public static Result<BinaryMessage> ParseMessage (this Header header, Span<byte> data)
	{
		switch (header.Identifier)
		{
			case HeaderIdentifier.TextMessage:
				return ParseTextMessage(header, data);
			case HeaderIdentifier.EventTableOfValueStates:
			case HeaderIdentifier.EventTableOfTextStates:
			case HeaderIdentifier.EventTableOfDaytimerStates:
			case HeaderIdentifier.EventTableOfWeatherStates:
				return ParseEventTableMessage(header, data);
			case HeaderIdentifier.OutOfService:
			case HeaderIdentifier.BinaryFile:
			case HeaderIdentifier.KeepAlive:
			default:
				return Result.Fail($"Could not parse {nameof(header.Identifier)} message");
		}
	}

	private static Result<BinaryMessage> ParseEventTableMessage (Header header, Span<byte> data)
	{
		switch (header.Identifier)
		{
			case HeaderIdentifier.EventTableOfValueStates:
				return ParseTableOfValues(header, data);
			case HeaderIdentifier.EventTableOfTextStates:
				return ParseTableOfText(header, data);
			case HeaderIdentifier.EventTableOfDaytimerStates:
				return ParseTableOfDayTimers(header, data);
			case HeaderIdentifier.EventTableOfWeatherStates:
				return ParseTableOfWeather(header, data);
			default:
				return Result.Fail($"Could not parse {nameof(header.Identifier)} message as event table.");
		}
	}

	private static Result<BinaryMessage> ParseTableOfWeather (Header header, Span<byte> data)
	{
		return Result.Fail("Table of weather messages are not implemented yet.");
	}

	private static Result<BinaryMessage> ParseTableOfDayTimers (Header header, Span<byte> data)
	{
		return Result.Fail("Table of daytimer messages are not implemented yet.");
	}

	private static Result<BinaryMessage> ParseTableOfText (Header header, Span<byte> data)
	{
		var message = new EventTableOfTextStates(header);
		var offset = 0;
		do
		{
			// read data
			var uuid = data.Slice(offset, 16).ParseUuid();
			var uuidIcon = data.Slice(offset + 16, 16).ParseUuid();
			var textLength = BitConverter.ToUInt32(data.Slice(offset + 32, 4));
			var textBinary = data.Slice(offset + 36, (int)textLength);
			var text = Encoding.UTF8.GetString(textBinary);
			var paddingBytes = (int)(textLength % 4);

			// calculate next offset
			offset += 16 + 16 + 4 + (int)textLength;
			if (paddingBytes > 0) offset += 4 - paddingBytes;

			// add item
			message.Values.Add(uuid, new TextEventData(uuidIcon, text));
		}
		while (offset < data.Length);
		return message;
	}

	private static Result<BinaryMessage> ParseTableOfValues (Header header, Span<byte> data)
	{
		const int itemSize = 24;
		var message = new EventTableOfValueStates(header);
		var valueCount = (data.Length / itemSize);
		for (var i = 0; i < valueCount; i++)
		{
			// read data
			var currentData = data.Slice(i * itemSize, itemSize);
			var uuid = currentData.Slice(0, 16).ParseUuid();
			var value = currentData.Slice(16, 8);
			var dValue = BitConverter.ToDouble(value);

			// add item
			message.Values.TryAdd(uuid, dValue);
		}
		return message;
	}

	private static Result<BinaryMessage> ParseTextMessage (Header header, Span<byte> data)
	{
		return Result.Fail("Text messages are not implemented yet.");
	}
}