namespace Loxone.Net.Extensions;

public static class UuidExtensions
{
	public static string ParseUuid (this Span<byte> bytes)
	{
		if (bytes.Length != 16) throw new ArgumentException($"Uuid must have 16 bytes");

		var data1 = bytes.Slice(0, 4);
		var data2 = bytes.Slice(4, 2);
		var data3 = bytes.Slice(6, 2);
		var data4 = bytes.Slice(8, 8);

		data1.Reverse();
		data2.Reverse();
		data3.Reverse();
		
		var data1Str = data1.ByteArrayToString();
		var data2Str = data2.ByteArrayToString();
		var data3Str = data3.ByteArrayToString();
		var data4Str = data4.ByteArrayToString();

		return $"{data1Str}-{data2Str}-{data3Str}-{data4Str}";
	}
}