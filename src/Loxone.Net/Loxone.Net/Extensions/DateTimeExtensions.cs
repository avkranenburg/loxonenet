namespace Loxone.Net.Extensions;

public static class DateTimeExtensions
{
	public static DateTime ToDateTime (this int offset)
	{
		return new DateTime(2009, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(offset);
	}
}