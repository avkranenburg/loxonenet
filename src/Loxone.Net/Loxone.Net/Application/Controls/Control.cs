﻿using System.Diagnostics;
using FluentResults;
using Loxone.Net.Application.States;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;
using Loxone.Net.Extensions;

namespace Loxone.Net.Application.Controls;

// todo: add "control" to available states

public class Control : LoxoneObject
{
    /// <summary>
	/// Room assigned to the control
	/// </summary>
	public Room? Room { get; init; }

	/// <summary>
	/// Category assigned to the control
	/// </summary>
	public Category? Category { get; init; }

	/// <summary>
	/// List of all nested controls
	/// </summary>
	public List<Control> SubControls { get; init; } = new();

	/// <summary>
	/// List of all linked controls
	/// </summary>
	public List<Control> LinkedControls { get; init; } = new();

	/// <summary>
	/// List of all available states
	/// </summary>
	public List<State> States { get; }

	/// <summary>
	/// Indicates whether the control can be locked
	/// </summary>
	public bool Lockable { get; }

	/// <summary>
	/// Generic event if any state of the control has changed
	/// </summary>
	public StateEventHandler? OnStateChanged;

	private State? _lockState { get; }

	internal Control (LoxoneControl control, LoxoneWebApi webApi, LoxoneApp app) : base (control.UuidAction, control.Name, webApi)
	{
		Room = app.Rooms.FirstOrDefault(x => x.Uuid == control.RoomUuid);
		Category = app.Categories.FirstOrDefault(x => x.Uuid == control.CategoryUuid);
		SubControls = control.SubControls?.Select(x => x.Value.Convert(app, webApi)).ToList() ?? [];
		States = control.States.Select(s => new State(s.Value, s.Key)).ToList();
		Lockable = (control.Details?.ContainsKey("jLockable") ?? false) && control.Details["jLockable"].GetBoolean();

		if (Lockable)
			_lockState = States.FirstOrDefault(s => s.Name == "jLocked");
	}

	protected async Task<Result> SendCommandAsync (string command)
	{
		var result = await WebApi.WriteValue(Uuid, command);
		return result.IsFailed ? result.ToResult() : Result.OkIf(result.Value == "1", "Command failed");
	}

	public async Task Lock (string reason = "")
	{
		if (Lockable)
			await SendCommandAsync($"lockcontrol/true/{Uri.EscapeDataString(reason)}");
	}

	public async Task Unlock ()
	{
		if (Lockable)
			await SendCommandAsync("unlockcontrol");
	}

	internal bool HasState (string uuid)
	{
		return States.Any(s => s.Uuid == uuid);
	}

	internal void SetState (string uuid, object value)
	{
		var state = States.FirstOrDefault(s => s.Uuid == uuid);
		if (state == null || state.SameValue(value)) return;

		state.Value = value;
		OnStateChanged?.Invoke(this, state);
	}

	/// <summary>
	/// Indicated whether the control is currently locked.
	/// Returns false if the control cannot be locked.
	/// </summary>
	public bool IsLocked ()
	{
		if (!Lockable || _lockState == null) return false;
		return (_lockState.GetLockState()?.LockState ?? 0) > 0;
	}

	/// <summary>
	/// Retrieve the reason for the current lock
	/// </summary>
	/// <returns></returns>
	public string GetLockReason ()
	{
		if (!Lockable || _lockState == null) return string.Empty;
		return _lockState.GetLockState()?.Reason ?? string.Empty;
	}
}