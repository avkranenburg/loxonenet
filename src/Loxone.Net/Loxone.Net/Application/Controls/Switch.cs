﻿using Loxone.Net.Application.States;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;

namespace Loxone.Net.Application.Controls;

public class Switch : Control
{
    private readonly State _activeState;
    private readonly State? _lockedOnState;

    internal Switch (LoxoneControl control, LoxoneWebApi webApi, LoxoneApp app) : base(control, webApi, app)
    {
        // find all relevant states and register them
        _activeState = States.First(s => s.Name == "active");
        _lockedOnState = States.FirstOrDefault(s => s.Name == "lockedOn");

        RegisterEvents();
    }

    // Commands

    /// <summary>
    /// Activates the switch.
    /// </summary>
    public async Task On () => await SendCommandAsync("on");

    /// <summary>
    /// Deactivates the switch.
    /// </summary>
    public async Task Off () => await SendCommandAsync("off");

    /// <summary>
    /// Toggle switch state.
    /// </summary>
    public async Task Toggle()
    {
        if (IsActive())
            await Off();
        else
            await On();
    }

    // States

    /// <summary>
    /// Returns true if the switch is active.
    /// </summary>
    public bool IsActive () => _activeState.GetBoolValue();

    /// <summary>
    /// Returns true if the switch is locked in the active state.
    /// </summary>
    public bool IsLockedOn () => _lockedOnState?.GetBoolValue() ?? false;

    // Events

    private void RegisterEvents ()
    {
        OnStateChanged += (_, state) =>
        {
            var handler = state.Name switch
            {
                "active" => state.GetBoolValue() ? OnSwitchActivated : OnSwitchDeactivated,
                _ => null
            };
            handler?.Invoke(this, state);
        };
    }

    public event StateEventHandler? OnSwitchActivated;

    public event StateEventHandler? OnSwitchDeactivated;
}