﻿using Loxone.Net.Application.States;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;

namespace Loxone.Net.Application.Controls;

public class Jalousie : Control
{
    private JalousieType Type { get; } = JalousieType.NotSupported;

    public bool IsAutomatic { get; }

    private readonly State _movingUpState;
    private readonly State _movingDownState;
    private readonly State _positionState;
    private readonly State _shadePositionState;
    private readonly State? _safetyActiveState;
    private readonly State? _autoAllowedState;
    private readonly State? _autoActiveState;

    internal Jalousie(LoxoneControl control, LoxoneWebApi webApi, LoxoneApp app) : base(control, webApi, app)
    {
        if (control.Details!["animation"].TryGetByte(out var type))
            Type = (JalousieType)type;

        IsAutomatic = control.Details!["isAutomatic"].GetBoolean();

        // find all relevant states and register them
        _movingUpState = States.First(s => s.Name == "up");
        _movingDownState = States.First(s => s.Name == "down");
        _positionState = States.First(s => s.Name == "position");
        _shadePositionState = States.First(s => s.Name == "shadePosition");

        // these states are only available for automatics
        if (IsAutomatic)
        {
            _safetyActiveState = States.FirstOrDefault(s => s.Name == "safetyActive");
            _autoAllowedState = States.FirstOrDefault(s => s.Name == "autoAllowed");
            _autoActiveState = States.FirstOrDefault(s => s.Name == "autoActive");
        }

        RegisterEvents();
    }

    // Commands

    /// <summary>
    /// Sends an Up command to the Jalousie, as long as no UpOff or other direction command has been sent the Jalousie will go Up.
    /// </summary>
    public async Task Up () => await SendCommandAsync("up");

    /// <summary>
    /// Sends an UpOff command to the Jalousie, this will stop the up motion of the Jalousie.
    /// </summary>
    public async Task UpOff () => await SendCommandAsync("UpOff");

    /// <summary>
    /// Sends a Down command to the Jalousie, as long as no DownOff or other direction command has been sent the Jalousie will go Down.
    /// </summary>
    public async Task Down () => await SendCommandAsync("down");

    /// <summary>
    /// Sends an DownOff command to the Jalousie, this will stop the down motion of the Jalousie.
    /// </summary>
    public async Task DownOff () => await SendCommandAsync("DownOff");

    /// <summary>
    /// Triggers a full up motion.
    /// </summary>
    public async Task FullUp () => await SendCommandAsync("FullUp");

    /// <summary>
    /// Triggers a full down motion.
    /// </summary>
    public async Task FullDown () => await SendCommandAsync("FullDown");

    /// <summary>
    /// Shades the Jalousie to the perfect position.
    /// </summary>
    public async Task Shade () => await SendCommandAsync("shade");

    /// <summary>
    /// Enables the Automatic of the Jalousie (if the control supports it).
    /// </summary>
    public async Task Auto ()
    {
        if (IsAutomatic)
            await SendCommandAsync("auto");
    }

    /// <summary>
    /// This disables the Automatic mode for the Jalousie (if the control supports it).
    /// </summary>
    public async Task NoAuto ()
    {
        if (IsAutomatic)
            await SendCommandAsync("NoAuto");
    }

    /// <summary>
    /// When received, the jalousie will move to the targetPosition provided.
    /// </summary>
    /// <param name="position">0 = fully open, 100 = fully closed</param>
    public async Task ManualPosition (int position) => await SendCommandAsync($"manualPosition/{position}");

    /// <summary>
    /// When received, the slats will rotate to this position.
    /// </summary>
    /// <param name="lamelleposition">0 = horizontal, 100 = vertical</param>
    public async Task ManualLamelle (int lamelleposition)
    {
        if (Type == JalousieType.Blinds)
            await SendCommandAsync($"manualLamelle/{lamelleposition}");
    }

    /// <summary>
    /// Combination of ManualPosition and ManualLamelle.
    /// </summary>
    /// <param name="position">0 = fully open, 100 = fully closed</param>
    /// <param name="lamelleposition">0 = horizontal, 100 = vertical</param>
    public async Task ManualPosBlind (int position, int lamelleposition)
    {
        if (Type == JalousieType.Blinds)
            await SendCommandAsync($"manualPosBlind/{position}/{lamelleposition}");
    }

    /// <summary>
    /// Immediately stops the position, remains in current position.
    /// </summary>
    public async Task Stop () => await SendCommandAsync("stop");

    // States

    /// <summary>
    /// Returns true if Jalousie is moving up.
    /// </summary>
    public bool IsMovingUp ()
    {
        return _movingUpState.GetBoolValue();
    }

    /// <summary>
    /// Returns true if Jalousie is moving down.
    /// </summary>
    public bool IsMovingDown ()
    {
        return _movingDownState.GetBoolValue();
    }

    /// <summary>
    /// Returns true if Jalousie is moving in any direction.
    /// </summary>
    public bool IsMoving ()
    {
        return IsMovingUp() || IsMovingDown();
    }

    /// <summary>
    /// Returns the current position of the jalousie.
    /// </summary>
    /// <returns>Value between 0 and 1</returns>
    public double GetPosition () => _positionState.GetDoubleValue();

    /// <summary>
    /// Returns the current position of the slats.
    /// </summary>
    /// <returns>Value between 0 and 1</returns>
    public double GetShadePosition () => _shadePositionState.GetDoubleValue();

    /// <summary>
    /// Returns if the safety shutdown is active. Only available if IsAutomatic is true.
    /// </summary>
    public bool IsSafetyActive () => IsAutomatic && (_safetyActiveState?.GetBoolValue() ?? false);

    /// <summary>
    /// Returns if turning on automatic mode is allowed. Only available if IsAutomatic is true.
    /// </summary>
    public bool IsAutoAllowed () => IsAutomatic && (_autoAllowedState?.GetBoolValue() ?? false);

    /// <summary>
    /// Returns if automatic mode is active. Only available if IsAutomatic is true.
    /// </summary>
    public bool IsAutoActive () => IsAutomatic && (_autoActiveState?.GetBoolValue() ?? false);

    // Events

    private void RegisterEvents ()
    {
        OnStateChanged += (_, state) =>
        {
            var handler = state.Name switch
            {
                "up" => state.GetBoolValue() ? OnStartedMoving : OnStoppedMoving,
                "down" => state.GetBoolValue() ? OnStartedMoving : OnStoppedMoving,
                "position" => OnPositionChanged,
                "shadePosition" => OnShadePositionChanged,
                _ => null
            };
            handler?.Invoke(this, state);
        };
    }

    public event StateEventHandler? OnStartedMoving;

    public event StateEventHandler? OnStoppedMoving;

    public event StateEventHandler? OnPositionChanged;

    public event StateEventHandler? OnShadePositionChanged;
}

internal enum JalousieType
{
    Blinds = 0,
    Shutters = 1,
    CurtainBothSides = 2,
    NotSupported = 3,
    CurtainLeft = 4,
    CurtainRight = 5,
}