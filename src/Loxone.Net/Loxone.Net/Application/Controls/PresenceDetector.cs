﻿using Loxone.Net.Application.States;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;

namespace Loxone.Net.Application.Controls;

public class PresenceDetector : Control
{
    private readonly State _activeSinceState;
    private readonly State _activeState;
    private readonly State _timeState;

    internal PresenceDetector(LoxoneControl control, LoxoneWebApi webApi, LoxoneApp app) : base(control, webApi, app)
    {
        // find all relevant states and register them
        _activeSinceState = States.First(s => s.Name == "activeSince");
        _activeState = States.First(s => s.Name == "active");
        _timeState = States.First(s => s.Name == "time");

        RegisterEvents();
    }

    // States

    /// <summary>
    /// Returns the Date & Time of when presence was activated.
    /// </summary>
    public DateTime ActiveSince => _activeSinceState.GetDateTimeValue();

    /// <summary>
    /// Returns if the presence output is active.
    /// </summary>
    public bool IsActive() => _activeState.GetBoolValue();

    /// <summary>
    /// Returns the current overrun time.
    /// </summary>
    public int OverrunTime() => _timeState.GetIntValue();

    // Events

    private void RegisterEvents()
    {
        OnStateChanged += (_, state) =>
        {
            var handler = state.Name switch
            {
                "active" => state.GetBoolValue() ? OnPresenceStart : OnPresenceEnd,
                _ => null
            };
            handler?.Invoke(this, state);
        };
    }

    public event StateEventHandler? OnPresenceStart;

    public event StateEventHandler? OnPresenceEnd;
}