﻿using Loxone.Net.Application.Controls;
using Loxone.Net.Connection;

namespace Loxone.Net.Application;

public class LoxoneApp
{
    private LoxoneWebApi _webApi;
    private DateTime _lastModified;

    public LoxoneApp (LoxoneWebApi webApi, DateTime lastModified)
    {
        _lastModified = lastModified;
        _webApi = webApi;
    }

    public List<Room> Rooms { get;  } = new();
    public List<Category> Categories { get;  } = new();
    public List<Control> Controls { get;  } = new();
    public Dictionary<int, string> OperatingModes { get; } = new();
    private Dictionary<string, Action<string, object>> _stateCache = new();

    public void SetState (string uuid, object value)
    {
        if (!_stateCache.ContainsKey(uuid))
        {
            var control = Controls.FirstOrDefault(c => c.HasState(uuid));
            if (control != null)
            {
                _stateCache[uuid] = control.SetState;
                control.SetState(uuid, value);
            }
            else
            {
                // check global states
            }
        }
        else
        {
            _stateCache[uuid](uuid, value);
        }
    }
}