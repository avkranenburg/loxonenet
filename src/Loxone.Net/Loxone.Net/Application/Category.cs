﻿using System.Drawing;
using Loxone.Net.Application.Controls;
using Loxone.Net.Connection;

namespace Loxone.Net.Application;

public class Category : LoxoneObject
{
    public Category(string uuid, string name, LoxoneWebApi webApi) : base(uuid, name, webApi)
    {

    }

    public List<Control> Controls { get; } = new();

    public required Color Color { get; init; }
}