﻿using Loxone.Net.Connection;

namespace Loxone.Net.Application;

public class LoxoneObject
{
    public string Uuid { get; }
    public string Name { get; }
    protected readonly LoxoneWebApi WebApi;

    public LoxoneObject (string uuid, string name, LoxoneWebApi webApi)
    {
        Uuid = uuid;
        Name = name;
        WebApi = webApi;
    }
}