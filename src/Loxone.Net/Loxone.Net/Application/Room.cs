﻿using Loxone.Net.Application.Controls;
using Loxone.Net.Connection;

namespace Loxone.Net.Application;

public class Room : LoxoneObject
{
    public List<Control> Controls { get; } = new();

    public Room(string uuid, string name, LoxoneWebApi webApi) : base(uuid, name, webApi)
    {

    }
}