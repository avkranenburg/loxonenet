﻿using System.Text.Json;

namespace Loxone.Net.Application.States;

public class LockStateValue
{
    [JsonPropertyName("locked")]
    public int LockState { get; set; }

    [JsonPropertyName("reason")]
    public string Reason { get; set; }
}

public static class LockStateExtension
{
    public static LockStateValue? GetLockState (this State state)
    {
        if (state.Value is string s)
            return JsonSerializer.Deserialize<LockStateValue>(s);

        return null;
    }
}