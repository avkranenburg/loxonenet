﻿using Loxone.Net.Extensions;

namespace Loxone.Net.Application.States;

public class State
{
    public State (string uuid, string name)
    {
        Uuid = uuid;
        Name = name;
    }

    public string Uuid { get; }
    public string Name { get; }

    public object? Value { get; set; }

    public bool GetBoolValue()
    {
        return (Value is bool value) && value;
    }

    public double GetDoubleValue()
    {
        return (Value is double value) ? value : 0;
    }

    public override bool Equals (object? obj) => obj is State state && Uuid == state.Uuid;

    public bool SameValue(object value)
    {
        return (value is bool newBool && GetBoolValue() == newBool)
            || (value is double newDouble && Math.Abs(GetDoubleValue() - newDouble) < 0.0001);
    }

    public DateTime GetDateTimeValue()
    {
        return (Value is int offset) ? offset.ToDateTime() : DateTime.MinValue;
    }

    public int GetIntValue()
    {
        return (Value is int value) ? value : 0;
    }
}