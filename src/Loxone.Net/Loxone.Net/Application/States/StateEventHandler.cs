﻿using Loxone.Net.Application.Controls;

namespace Loxone.Net.Application.States;

public delegate void StateEventHandler(Control control, State state);