﻿namespace Loxone.Net.Application.States;

public delegate void DoubleEventHandler(object sender, double args);