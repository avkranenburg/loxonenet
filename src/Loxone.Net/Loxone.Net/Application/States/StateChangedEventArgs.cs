﻿namespace Loxone.Net.Application.States;

public class StateChangedEventArgs : EventArgs
{
    public object? Value { get; }
    public StateChangedEventArgs(object? value)
    {
        Value = value;
    }
}