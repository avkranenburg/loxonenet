﻿using FluentResults;
using Loxone.Net.Application;
using Loxone.Net.Connection;
using Loxone.Net.Data.LoxApp;
using Loxone.Net.Data.Messages;
using Loxone.Net.Extensions;

namespace Loxone.Net;

public class Miniserver
{
    private bool _messageReceved = false;
    private LoxoneWebsocket _websocket { get; }
    private LoxoneWebApi _webApi { get; }
    public LoxApp3 LoxApp3 { get; private set; } = new();
    public LoxoneApp LoxoneApp { get; private set; }

    public Miniserver (string host, int port, string username, string password)
    {
        _websocket = new LoxoneWebsocket(host, port, username, password);
        _webApi = new LoxoneWebApi(host, port, username, password);

        // initialize event handlers
        _websocket.OnMessageReceived += MessageReceivedHandler;
    }

    public async Task<Result> ConnectAsync ()
    {
        // retrieve loxone app data, cancel connect if it fails
        var loxAppSync = await GetLoxAppAsync();
        if (loxAppSync.IsFailed)
            return loxAppSync;

        LoxoneApp = LoxApp3.CreateApp(_webApi);

        // connect the websocket
        await _websocket.ConnectAsync();

        // wait for authentication to complete
        while (!_websocket.IsConnected)
            await Task.Delay(50);

        _websocket.EnableStatusUpdates();

        return Result.Ok();
    }

    private void MessageReceivedHandler (object? sender, OnMessageEventArgs e)
    {
        switch (e.Message)
        {
            case EventTableOfDaytimerStates daytimerStates:
                break;
            case EventTableOfTextStates textStates:
                foreach (var textState in textStates.Values)
                    LoxoneApp.SetState(textState.Key, textState.Value.Text);
                break;
            case EventTableOfValueStates valueStates:
                foreach (var valueState in valueStates.Values)
                    LoxoneApp.SetState(valueState.Key, valueState.Value);
                break;
            case EventTableOfWeatherStates weatherStates:
                break;
        }

        _messageReceved = true;
    }

    private async Task<Result> GetLoxAppAsync ()
    {
        var result = await _webApi.GetAsync<LoxApp3>("data/LoxAPP3.json");
        if (!result.IsSuccess) return result.ToResult();
        LoxApp3 = result.Value;
        return Result.Ok();
    }

    public void Dispose ()
    {
        _websocket.Close();
    }

    public async Task WaitForInitialStatus(TimeSpan timeOut, CancellationToken token = default)
    {
        var start = DateTime.Now;
        while (!_messageReceved && !token.IsCancellationRequested && DateTime.Now - start < timeOut)
            await Task.Delay(100, token);
    }
}